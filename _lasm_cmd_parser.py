#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions an
# limitations under the License.
#
#
# LASM Command Parser
#
#   Search a command and convert into 16bit code
#   Written based on documents below
#     - www.ti.com/lit/ds/symlink/lp5523.pdf
#     - www.ti.com/lit/an/snva664/snva664.pdf
#

import six                    # support compatibility between Python 2.x and 3
import _lasm_utils as util

# Constant numbers for ramp/wait command
PRESCALE0 = 0.015625
PRESCALE1 = 0.5
MASTER_FREQ = 32768
PRESCALE0_CLK = MASTER_FREQ / 16
PRESCALE1_CLK = MASTER_FREQ / 512
MAXCODE = 0x1F
PRESCALE_CODE = 0x40


# Display error
# Format: 'function name' 'error message'
def put_error(message):
    func_name = util.get_caller_func_name()
    util.put_error(func_name, message)
    return False


# Segment command is used for program engine selection (1,2,3)
# Save start SRAM address to calculate label offset
def segment(name):
    args = name.split()
    if len(args) != 1:
        return put_error('must have one argument')

    if util.add_segment_list(args[0]) is False:
        return put_error('duplicate segment name')

    return True


def mux_map_start(label):
    args = label.split()
    if len(args) != 1:
        return put_error('must have one argument')

    code = util.get_address_of_label(args[0], '')
    if code == util.INVALID_ADDR:
        return False

    MUX_MAP_START = 0x9C00
    return util.put_code(MUX_MAP_START | code)


def mux_map_next(val):
    return util.put_code(0x9D80)


def mux_map_prev(val):
    return util.put_code(0x9DC0)


def mux_ld_next(val):
    return util.put_code(0x9D81)


def mux_ld_prev(val):
    return util.put_code(0x9DC1)


def mux_ld_addr(label):
    args = label.split()
    if len(args) != 1:
        return put_error('must have one argument')

    code = util.get_address_of_label(args[0], '')
    if code == util.INVALID_ADDR:
        return False

    MUX_LD_ADDR = 0x9F00
    return util.put_code(MUX_LD_ADDR | code)


def mux_map_addr(label):
    args = label.split()
    if len(args) != 1:
        return put_error('must have one argument')

    code = util.get_address_of_label(args[0], '')
    if code == util.INVALID_ADDR:
        return False

    MUX_MAP_ADDR = 0x9F80
    return util.put_code(MUX_MAP_ADDR | code)


def mux_sel(val):
    args = val.split()
    if len(args) != 1:
        return put_error('must have one argument')

    MAX_LEDS = 9
    GPO = 16
    led = int(args[0])
    if (led < 0 or (led > MAX_LEDS and led is not GPO)):
        return put_error('%d is invalid argument' % led)

    MUX_SEL = 0x9D00
    return util.put_code(MUX_SEL | led)


def mux_clr(val):
    return util.put_code(0x9D00)


def mux_ld_start(label):
    args = label.split()
    if len(args) != 1:
        return put_error('must have one argument')

    code = util.get_address_of_label(args[0], '')
    if code == util.INVALID_ADDR:
        return False

    MUX_LD_START = 0x9E00
    return util.put_code(MUX_LD_START | code)


def mux_ld_end(label):
    args = label.split()
    if len(args) != 1:
        return put_error('must have one argument')

    code = util.get_address_of_label(args[0], '')
    if code == util.INVALID_ADDR:
        return False

    MUX_LD_END = 0x9C80
    return util.put_code(MUX_LD_END | code)


def set_pwm(val):
    args = val.split()
    if len(args) != 1:
        return put_error('must have one argument')

    arg = args[0]
    # Format: set_pwm 'ra' or 'rb' or 'rc' or 'rd'
    #         set_pwm <number>
    variables = {'ra': 0, 'rb': 1, 'rc': 2, 'rd': 3}
    index_var = variables.get(arg)
    if index_var is None:
        if arg.isdigit() is False:
            t = util.number_type(arg)
            types = {'b', 'B', 'h', 'H'}
            if t not in types:
                return put_error('%s is invalid argument' % arg)

        SET_PWM = 0x4000
        code = util.format_number(arg)
        return util.put_code(SET_PWM | code)

    SET_PWM = 0x8460
    return util.put_code(SET_PWM | index_var)


def wait(val):
    args = val.split()
    if len(args) != 1:
        return put_error('must have one argument')

    time = float(args[0])
    if time < 0:
        return put_error('time should be positive value')

    if time < PRESCALE0:
        code = min(int(round(time * PRESCALE0_CLK, 0)), MAXCODE)
        code = code << 1
    elif time < PRESCALE1:
        code = min(int(round(time * PRESCALE1_CLK, 0)), MAXCODE)
        code = PRESCALE_CODE | (code << 1)
    else:
        return put_error('time should be less than 0.48')

    return util.put_code(code << 8)


# Format: ramp <time>, <pwm>
#         ramp <variable1>, <prescale>, <variable2>
#              variable1 = 'ra' or 'rb' or 'rc' or 'rd'
#              prescale  = 'pre=0' or 'pre=1'
#              variable2 = '+ra' or '+rb' or '+rc' or '+rd' or
#                          '-ra' or '-rb' or '-rc' or '-rd'
def ramp(val):
    args = val.split(',')
    if len(args) == 2:        # format : ramp <time>, <pwm>
        # Remove space and check whether it is digit or not
        stime = args[0].split()[0]
        spwm = args[1].split()[0]
        if util.is_float(stime):
            time = float(stime)
            pwm = int(spwm)

            if time < 0:
                return put_error('time should be positive value')

            if (pwm < -255 or pwm > 255):
                return put_error('%d is out of range. -255 <= value <= 255'
                                  % pwm)

            if pwm == 0:
                return put_error('%d is invalid. should not zero' % pwm)

            time = time / abs(pwm)
            if time < PRESCALE0:
                code = min(int(round(time * PRESCALE0_CLK, 0)), MAXCODE)
                code = code << 1
            elif time < PRESCALE1:
                code = min(int(round(time * PRESCALE1_CLK, 0)), MAXCODE)
                code = PRESCALE_CODE | (code << 1)
            else:
                return put_error('too long time')

            NEGATIVE = 0x01
            if pwm < 0:
                code = code | NEGATIVE

            return util.put_code((code << 8) | abs(pwm))
        else:
            return put_error('invalid value')
    elif len(args) == 3:        # format : ramp <var1>, <prescale>, <var2>
        # Step time
        var1 = args[0].split()[0]
        variables = {'ra': 0x0, 'rb': 0x04, 'rc': 0x08, 'rd': 0x0C}
        step_time = variables.get(var1)
        if step_time is None:
            return put_error('%s is invalid argument' % var1)

        # Prescale
        if args[1].split()[0] == 'pre=0':
            prescale = 0 << 4
        elif args[1].split()[0] == 'pre=1':
            prescale = 1 << 4
        else:
            return put_error('%s is invalid argument' % args[1].split()[0])

        # Sign bit and number of increments
        var2 = args[2].split()[0]
        variables = {'+ra': 0x00, '+rb': 0x01, '+rc': 0x02, '+rd': 0x03,
                     '-ra': 0x10, '-rb': 0x11, '-rc': 0x12, '-rd': 0x13}
        sign_and_inc = variables.get(var2)
        if sign_and_inc is None:
            return put_error('%s is invalid argument' % var2)

        RAMP = 0x8400
        return util.put_code(RAMP | prescale | sign_and_inc | step_time)

    return put_error('invalid arguments')


# Convert ds argument to offset
def get_offset_size(val):
    args = val.split()
    if len(args) != 1:
        return -1
    return util.format_number(args[0])


def ds(val):
    size = get_offset_size(val)
    if size < 0:
        return put_error('must have one argument')

    for i in range(0, size):
        util.put_code(0x0000)
    return True


def dw(val):
    args = val.split()
    if len(args) != 1:
        return put_error('must have one argument')

    code = util.format_number(args[0])
    return util.put_code(code)


# Format: branch <number>, label
#         branch 'ra' or 'rb' or 'rc' or 'rd', label
def branch(val):
    args = val.split(',')
    if len(args) != 2:
        return put_error('must have two arguments')

    # Remove space and check whether it is digit or not
    if args[0].split()[0].isdigit():
        loop_count = int(args[0].split()[0])
        if (loop_count < 0 or loop_count > 64):
            return put_error('%d is out of range. 0 <= loop count <= 63'
                              % loop_count)

        label = args[1].split()[0]
        step_no = util.get_offset_address_of_label(label)
        if step_no == util.INVALID_ADDR:
            return False

        BRANCH = 0xA000
        word = (BRANCH | (loop_count << 7) | (step_no & 0x7F))
    else:
        variables = {'ra': 0, 'rb': 1, 'rc': 2, 'rd': 3}
        index_var = variables.get(args[0].split()[0])
        if index_var is None:
            return put_error('%s is invalid argument' % args[0].split()[0])

        label = args[1].split()[0]
        step_no = util.get_offset_address_of_label(label)
        if step_no == util.INVALID_ADDR:
            return False

        BRANCH = 0x8600
        word = (BRANCH | (step_no << 2) | index_var)

    return util.put_code(word)


def reset(val):
    return util.put_code(0x0000)


def interrupt(val):
    return util.put_code(0xC400)


def end(val):
    args = val.split()
    if len(args) == 0:    # format: 'end'
        return util.put_code(0xC000)

    args = args[0].split(',')
    num_args = len(args)
    if num_args == 1:        # format: 'end i' or 'end r'
        arg = args[0]
        if arg == 'i':
            return util.put_code(0xD000)
        elif arg == 'r':
            return util.put_code(0xC800)
    elif num_args == 2:        # format: 'end i,r' or 'end r,i'
        arg1 = args[0]
        arg2 = args[1]
        if (arg1 == 'i' and arg2 == 'r') or (arg1 == 'r' and arg2 == 'i'):
            return util.put_code(0xD800)

    return put_error('invalid argument')


# Generate trigger code
def get_trigger_code(val):
    code = 0
    if (val.find('1') > 0):
        code |= 0x01
    if (val.find('2') > 0):
        code |= 0x02
    if (val.find('3') > 0):
        code |= 0x04
    if (val.find('e') > 0):
        code |= 0x20

    return code


# Format: trigger w{source1 | source 2 .. }
#         trigger s{source1 | source 2 .. }
#         trigger w{source1 | source 2 .. }, s{source1 | source 2 .. }
#         trigger s{source1 | source 2 .. }, w{source1 | source 2 .. }
#         trigger w{source1 | source 2 .. } s{source1 | source 2 .. }
#         trigger s{source1 | source 2 .. } w{source1 | source 2 .. }
def trigger(val):
    WAIT = 'w{'
    START = 's{'
    w_code = 0
    s_code = 0

    idx_wait = val.find(WAIT)
    if idx_wait > 0:
        w_code = get_trigger_code(val[idx_wait:])

    idx_start = val.find(START)
    if idx_start > 0:
        s_code = get_trigger_code(val[idx_start:])

    if idx_wait < 0 and idx_start < 0:
        return put_error('invalid argument')

    TRIGGER = 0xE000
    return util.put_code(TRIGGER | (w_code << 7) | (s_code << 1))


# Common function for jne, jl, jge and je
# Format: <command> var1, var2, label(address)
def jump(val, action):
    args = val.split(',')
    if len(args) != 3:
        return put_error('must have three arguments')

    # Check the 1st and 2nd argument are variable or not
    var1 = args[0].split()[0]
    var2 = args[1].split()[0]
    variables = {'ra': 0, 'rb': 1, 'rc': 2, 'rd': 3}
    index_var1 = variables.get(var1)
    if index_var1 is None:
        return put_error('%s is invalid argument' % var1)

    index_var2 = variables.get(var2)
    if index_var2 is None:
        return put_error('%s is invalid argument' % var2)

    # Check the 3rd is label or not
    var3 = args[2].split()[0]
    addr = util.get_address_of_label(var3, "")
    if addr == util.INVALID_ADDR:
        return False

    cmds = {'jne': 0x8800, 'jl': 0x8A00, 'jge': 0x8C00, 'je': 0x8E00}
    code = cmds.get(action)
    if code is None:
        return put_error('%s is invalid argument' % action)

    offset = abs(addr - util.get_sram_address()) - 1
    word = (code | (offset << 4) | (index_var1 << 2) | index_var2)
    return util.put_code(word)


def jne(val):
    return jump(val, 'jne')


def jl(val):
    return jump(val, 'jl')


def jge(val):
    return jump(val, 'jge')


def je(val):
    return jump(val, 'je')


def ld(val):
    args = val.split(',')
    if len(args) != 2:
        return put_error('must have two arguments')

    # Check the 1st argument is variable or not
    var = args[0].split()[0]
    target = {'ra': 0, 'rb': 1, 'rc': 2}
    index = target.get(var)
    if index is None:
        return put_error('%s is invalid argument' % var)

    # Check the 2nd argument is valid or not
    val = util.format_number(args[1].split()[0])
    if val < 0 or val > 255:
        return put_error('%d is out of range. 0 <= value <=255' % val)

    LD = 0x9000
    word = (LD | (index << 10) | val)
    return util.put_code(word)


# Common function for 'add' and 'sub'
# Two formats supported
#   <command> var, value
#   <command> var1, var2, var3
def add_or_sub(val, action):
    args = val.split(',')
    if len(args) == 2:          # format: add var, value
        # Check the 1st argument is variable or not
        var = args[0].split()[0]
        target = {'ra': 0, 'rb': 1, 'rc': 2}
        index = target.get(var)
        if index is None:
            return put_error('%s is invalid argument' % var)

        # Check the 2nd argument is valid or not
        val = util.format_number(args[1].split()[0])
        if (val < 0 or val > 255):
            return put_error('%d is out of range. 0 <= value <=255' % val)

        cmds = {'add': 0x9100, 'sub': 0x9200}
        code = cmds.get(action)
        if code is None:
            return put_error('%s is invalid argument' % action)

        word = (code | (index << 10) | val)
        util.put_code(word)
    elif len(args) == 3:        # format: add var1, var2, var3
        # Check the 1st argument is variable or not
        var1 = args[0].split()[0]
        target = {'ra': 0, 'rb': 1, 'rc': 2}
        index_var1 = target.get(var1)
        if index_var1 is None:
            return put_error('%s is invalid argument' % var1)

        # Check the others are variable or not
        var2 = args[1].split()[0]
        var3 = args[2].split()[0]
        others = {'ra': 0, 'rb': 1, 'rc': 2, 'rd': 3}
        index_var2 = others.get(var2)
        if index_var2 is None:
            return put_error('%s is invalid argument' % var2)

        index_var3 = others.get(var3)
        if index_var3 is None:
            return put_error('%s is invalid argument' % var3)

        cmds = {'add': 0x9300, 'sub': 0x9310}
        code = cmds.get(action)
        if code is None:
            return put_error('%s is invalid argument' % action)

        word = (code | (index_var1 << 10) | (index_var2 << 2) | index_var3)
        return util.put_code(word)
    else:
        return put_error('invalid arguments')


def add(val):
    return add_or_sub(val, "add")


def sub(val):
    return add_or_sub(val, "sub")


# Command map
# Format: { 'command':function }
# If 'command' is found, then function is called by lookup_command() in case
# the action is 'generate_code'
commands = {
            '.segment': segment,
            'mux_map_start': mux_map_start,
            'mux_map_next': mux_map_next,
            'mux_map_prev': mux_map_prev,
            'mux_ld_next': mux_ld_next,
            'mux_ld_prev': mux_ld_prev,
            'mux_ld_addr': mux_ld_addr,
            'mux_map_addr': mux_map_addr,
            'mux_sel': mux_sel,
            'mux_clr': mux_clr,
            'mux_ld_start': mux_ld_start,
            'mux_ld_end': mux_ld_end,
            'set_pwm': set_pwm,
            'wait': wait,
            'ramp': ramp,
            'ds': ds,
            'dw': dw,
            'branch': branch,
            'rst': reset,
            'int': interrupt,
            'end': end,
            'trigger': trigger,
            'jne': jne,
            'jl': jl,
            'jge': jge,
            'je': je,
            'ld': ld,
            'add': add,
            'sub': sub,
            }


# Command Parser
#
# Format: <command> <argument1> .. <argumentsN>
# To ignore spaces, split() is used
def lookup_command(cmd, action):
    # Skip checking if the command is space character
    if len(cmd.split()) == 0:
        return True

    for key, func in six.iteritems(commands):
        if cmd.split()[0] == key:
            # Two actions are allowed when the command is matched
            # - create_label: just increase SRAM address for next use
            # - generate_code: code is generated by each function call
            if action == 'create_label':
                # .segment command does not require SRAM address increment
                if key == '.segment':
                    return True

                # ds command needs to update SRAM size offset
                if key == 'ds':
                    arg = cmd.split(key)[1]
                    size = get_offset_size(arg)
                    if size < 0:
                        return put_error("wrong size information")

                    for x in range(0, size):
                        if util.inc_sram_address() is False:
                            return put_error("code size should be less than %d"
                                              % util.MAX_SRAM_SIZE)

                    return True

                # Other commands, just increase SRAM address
                if util.inc_sram_address() is False:
                    return put_error("code size should be less than %d"
                                      % util.MAX_SRAM_SIZE)

                return True
            elif action == 'generate_code':
                arg = cmd.split(key)[1]
                return func(arg)        # call a function

    return put_error("%s command is not found" % cmd)


# Line format
#
# COMMAND ARGUMENTS(S)              'COMMAND format'
# LABEL:    COMMAND ARGUMENT(S)     'LABEL format'
def parse_line(string, action):
    LABEL = ":"
    index = string.find(LABEL)
    if index > 0:    # LABEL format
        if action == 'create_label':
            util.add_label_list(string[:index])
        return lookup_command(string[index + 1:], action)
    else:              # COMMAND format
        return lookup_command(string, action)
