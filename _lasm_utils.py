#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions an
# limitations under the License.
#
#
# Utility functions for LASM
#
#   Common functions used in main entry and command parser
#

import inspect
import six           # support compatibility between Python 2.x and 3

segment_addr = 0     # mapping segment to SRAM address
sram_addr = 0        # LP5523 SRAM offset from 0 to 95
MAX_SRAM_SIZE = 96
INVALID_ADDR = 0xFF

segments = {}        # { "segment":address}
labels = {}          # { "key":address }
errors = {}          # { command: error }
codes = []           # output program code

LF = '\n'
CRLF = '\r\n'        # .hex should be Windows format due to LP5523.exe


#
# Error handling functions
#

# Make error format
def put_error(command, message):
    errors.update({command: message})


# Print out errors
def report_error(ln, line):
    for cmd, msg in six.iteritems(errors):
        six.print_(
        """
        Error! Line number: %d
        %s
        [%s] %s
        """ % (ln, line, cmd, msg)
        )


#
# Segment functions
#

# Initialize segment list
def init_segment():
    global segment_addr, segments
    segment_addr = 0
    segments = {}


# Add segment structure
# Format: { segment name:address }
def add_segment_list(name):
    for x in segments:
        if segments.get(name) is not None:
            return False

    global segment_addr
    segment_addr = get_sram_address()
    segments.update({name: segment_addr})
    return True


# Return the address of the latest segment
def get_segment_address():
    global segment_addr
    return segment_addr


#
# SRAM functions
#

# Initialize SRAM
def init_sram_address():
    global sram_addr
    sram_addr = 0


# Increase SRAM address
# Return false if address is out of range
def inc_sram_address():
    global sram_addr
    sram_addr += 1

    if sram_addr > MAX_SRAM_SIZE:
        return False
    else:
        return True


# Get SRAM address
def get_sram_address():
    global sram_addr
    return sram_addr


#
# Program engine code functions
#

# Initialize code
def init_code():
    global codes
    codes = []


# Put word bytes
# Return true/false
def put_code(word):
    codes.append((word & 0xFF00) >> 8)
    codes.append(word & 0x00FF)
    return inc_sram_address()


#
# Label functions
#

# Initialize label list
def init_label_list():
    global labels
    labels = {}


# Add label information
# Format: { label name:address }
def add_label_list(name):
    key = name.split()[0]
    addr = get_sram_address()
    labels.update({key: addr})


# Get address of label
# Invalid address returns if the label is not found
def get_address_of_label(label, func_name):
    for key in labels:
        if label == key:
            return labels.get(key)

    if not func_name:
        func_name = get_caller_func_name()

    put_error(func_name, 'can not find \'%s\'' % label)
    return INVALID_ADDR


# Calculate offset address
# Returns (label address - segment address)
def get_offset_address_of_label(key):
    addr = get_address_of_label(key, get_caller_func_name())
    if addr == INVALID_ADDR:
        return INVALID_ADDR

    offset = addr - get_segment_address()
    if offset < 0:
        return INVALID_ADDR

    return offset


# Trace the stack to get name of the caller
def get_caller_func_name():
    return inspect.stack()[2][3]


#
# Output file generation functions
#

# Convert value to binary type - fill with zeros
def bin_format(val, bit):
    return '{:b}'.format(val).zfill(bit)


# Convert value to hex type - fill with zero
def hex_format(val):
    return '{:X}'.format(val).zfill(2)


# Generate binary file
# Format:
#       segment code
#       sram code (up to 96 * 2 byte)
def generate_bin(src):
    fname = src[:-3] + 'bin'
    f = open(fname, 'w')

    # Update segment address
    for addr in sorted(segments.values()):
        bits = bin_format(addr, 8)
        f.write(bits)
        f.write(LF)

    # Fill the default segment value
    MAX_SEGMENTS = 3
    DEFAULT_SEG_VAL = MAX_SRAM_SIZE - 1
    for x in range(len(segments), MAX_SEGMENTS):
        bits = bin_format(DEFAULT_SEG_VAL, 8)
        f.write(bits)
        f.write(LF)

    # Update word code
    count = 0
    sram_size = 0
    for x in codes:
        bits = bin_format(x, 8)
        f.write(bits)
        count += 1
        if count % 2 == 0:
            f.write(LF)
            sram_size += 1

    # Fill zero for last area
    for n in range(sram_size, MAX_SRAM_SIZE):
        f.write(bin_format(0, 16))
        f.write(LF)

    f.close()


# Generate hex file
# Format:
#       sram code (up to 96 * 2 byte)
#       @ <segment address> <segment name>
def generate_hex(src):
    fname = src[:-3] + 'hex'
    f = open(fname, 'w')

    # Update byte code
    count = 0
    sram_size = 0
    for x in codes:
        f.write(hex_format(x))
        f.write(' ')
        count += 1
        if count % 2 == 0:
            sram_size += 1
        if count % 16 == 0:
            f.write(CRLF)

    # Fill zero for last area
    for n in range(sram_size, MAX_SRAM_SIZE):
        for a in range(0, 2):        # make two bytes
            f.write(hex_format(0))
            f.write(' ')
            count += 1
            if count % 16 == 0:
                f.write(CRLF)

    # Update segment name and address
    for addr in sorted(segments.values()):
        segname = [key for key, value in six.iteritems(segments)
                if value == addr][0]
        form = "@ %s %s" % (hex_format(addr), segname)
        f.write(form)
        f.write(CRLF)

    f.close()


#
# Other utils
#

# Get number type by reading suffix
def number_type(val):
    return val[-1]


# Check the value is float type or not
def is_float(val):
    try:
        float(val)
        return True
    except ValueError:
        return False


# XXXXb or XXXXB is binary
# XXXXh or XXXXH is hex
# XXXX is decimal
def format_number(val):
    t = number_type(val)
    if t == "b" or t == "B":        # binary
        code = int(val[:-1], 2)
    elif t == "h" or t == "H":      # hex
        code = int(val[:-1], 16)
    else:                           # decimal
        code = int(val, 10)
    return code