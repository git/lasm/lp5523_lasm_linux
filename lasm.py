#
# Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions an
# limitations under the License.
#
#
# LASM (LP5523 Assembler) Main Entry
#
#   LASM is used for generating program code which runs LP5523 chip
#
#   (input)             (output)
#   *.src -> lasm.py -> *.bin and *.hex
#

import os
import six                    # support compatibility between Python 2.x and 3
import sys
import _lasm_cmd_parser as cp
import _lasm_utils as util

NUM_ARGS = 3
COMMENT = ";"


# Check whether the line starts with comment or not
def is_comment(line):
    if line[0] == COMMENT:
        return True
    else:
        return False


# Get line string by removing comment
def readline_without_comment(line):
    index = line.find(COMMENT)
    if index > 0:
        line = line[:index]

    return line


# Error format
def error(argv):
    six.print_(
    """
    Argument error: %s
    Usage: python lasm.py -f <src file name>
           python lasm.py -d <directory name>
    """ % str(argv)
    )


# Initialize data structures
def init_data():
    util.init_label_list()
    util.init_segment()
    util.init_code()


# Parse *.src file by each line
def process_from_file(src):
    init_data()
    # Add label info first and then generate code
    for action in ['create_label', 'generate_code']:
        util.init_sram_address()
        ln = 0     # line number

        # Read line
        for line in open(src):
            ln += 1
            if is_comment(line):
                continue
            line = readline_without_comment(line)
            ret = cp.parse_line(line, action)
            if ret is False:
                util.report_error(ln, line)
                break

        if ret is False:
            return False

    # Generate output files
    util.generate_bin(src)
    util.generate_hex(src)
    return True


# Parse all .src files under a directory
def process_from_directory(dir):
    for f in os.listdir(dir):
        fullpath = dir + f
        if os.path.isfile(fullpath) and f[-3:] == 'src':
            if process_from_file(fullpath) is False:
                return False

    return True


def main():
    if len(sys.argv) != NUM_ARGS:
        error(sys.argv)
        exit()

    flag = sys.argv[1]
    if flag == '-f':            # file
        if process_from_file(sys.argv[2]) is False:
            exit()
    elif flag == '-d':          # directory
        if process_from_directory(sys.argv[2]) is False:
            exit()
    else:
        error(sys.argv)
        exit()

if __name__ == "__main__":
    main()
